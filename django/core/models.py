#-*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.contrib.gis.db import models as geoModels
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import D
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from . import exceptions

import googlemaps


class ClientCagnotteManager(models.Model):    
	''' Manager pour la cagnotte du client '''

	class Meta:
		abstract = True
	
	current_balance = models.PositiveSmallIntegerField(
		'montant actuel de la cagnotte',
		default=0)

	def debit(self, amount):
		''' TODO : à tester '''
		if amount > self.current_balance:
			raise exceptions.CantDebitIfNotEnoughBalance_Error()
		self.current_balance -= amount

	def credit(self, amount):
		''' TODO : à tester '''
		self.current_balance += amount
	
	def can_debit_amount(self, amount):
		''' TODO : à tester '''
		return amount <= self.current_balance


class ClientGamelleManager(models.Model):
	''' Manager pour les gamelles du client '''
	
	class Meta:
		abstract = True

	nbr_gamelles_dirty = models.PositiveSmallIntegerField(
		'nombre de gamelles sales à disposition', default=0)
	nbr_gamelles_brought_back = models.PositiveSmallIntegerField(
		'nombre de gamelles rapportées', default=0)
	nbr_gamelles_used = models.PositiveSmallIntegerField(
		'nombre de gamelles utilisées (total)', default=0)

	def borrow_gamelle(self, nbr_of_gamelles):
		''' 
		Lorsque le client emprunte une ou plusieurs gamelles au restaurant
		TODO : à tester '''
		self.nbr_gamelles_dirty += nbr_of_gamelles
		self.nbr_gamelles_used += nbr_of_gamelles

	def bring_back_gamelle(self, nbr_of_gamelles):
		''' 
		Lorsque le client ramène une ou pusieurs gamelles sales au restaurant
		TODO : à tester 
		'''
		if self.nbr_gamelles_dirty < nbr_of_gamelles:
			raise exceptions.CantBringBackGamelleYouDontHave_Error()
		self.nbr_gamelles_brought_back += nbr_of_gamelles
		self.nbr_gamelles_dirty -= nbr_of_gamelles



class RestaurantGamelleManager(models.Model):
	''' Manager pour les gamelles du restaurateur '''

	class Meta:
		abstract = True

	nbr_gamelles_available = models.PositiveSmallIntegerField(
		'nombre de gamelles propres disponibles', default=0)
	nbr_gamelles_dirty_brought_back = models.PositiveSmallIntegerField(
		'nombre de gamelles sales rapportées au restaurant (total)', default=0)
		
	def give_gamelle(self, nbr_of_gamelles):
		''' 
		Lorsque le restaurant prête une ou pusieurs gamelles à un client
		TODO : à tester 
		'''
		if self.nbr_gamelles_available < nbr_of_gamelles:
			raise exceptions.NotEnoughGamellesToGive_Error()
		self.nbr_gamelles_available -= nbr_of_gamelles
		
	def bring_back_gamelle(self, nbr_of_gamelles):
		''' 
		Lorsque le client ramène une ou pusieurs gamelles sales au restaurant
		TODO : à tester 
		'''
		self.nbr_gamelles_dirty_brought_back += nbr_of_gamelles


class Location(geoModels.Model):

	class Meta:
		abstract = True
	
	formatted_address = models.CharField('Adresse', 
		max_length=300, default="Non renseignée")
	geo_coord = geoModels.PointField('coordonnées géographiques',
		blank=True, null=True, srid=4326, geography=True, default=Point(48.8534100,2.3488000))

	country = models.CharField('country',
		max_length=100, default="France") 
	administrative_area_level_1 = models.CharField('administrative_area_level_1', 
		max_length=100, default="", blank=True)
	administrative_area_level_2 = models.CharField('administrative_area_level_2',
		max_length=100, default="", blank=True)
	postal_code = models.CharField('postal code',
		max_length=15, blank=True, null=True)
	locality = models.CharField('locality',
		max_length=100, default="")
	route = models.CharField('route',
		max_length=150, default="", blank=True)
	street_number = models.CharField('street_number',
		max_length=15, default="1", blank=True)
	
	nbr_updates_of_all_location = models.PositiveSmallIntegerField(
		"nombre de màj de location réalisées (sollicitation Google Maps)",
		default=0)

	def get_geocode(self, address):
		gmaps = googlemaps.Client(key='AIzaSyBKp9ALNx1jXuB5r43koPtJYyu2jMuhjto')
		return gmaps.geocode(address=address, language='fr')[0]

	def update_formatted_address(self, geocode):
		""" Mise à jour de l'adresse formatée par Google Maps."""
		if 'formatted_address' not in geocode:
			raise exceptions.GeocodeIsCorrupted_Error(param='formatted_address')
		self.formatted_address = geocode['formatted_address']

	def update_geo_coord(self, geocode):
		""" Mise à jour des coordonnées géographiques fournies par Google Maps."""
		if 'geometry' not in geocode:
			raise exceptions.GeocodeIsCorrupted_Error(param='geometry')
		self.geo_coord = Point(
			geocode['geometry']['location']['lng'],
			geocode['geometry']['location']['lat'],
			srid=4326)

	def update_address_components(self, geocode):
		""" Mise à jour des différentes éléments propres à l'adresse."""
		if 'address_components' not in geocode:
			raise exceptions.GeocodeIsCorrupted_Error(param='address_components')
		for component in geocode['address_components']:
			if 'types' in component:
				for component_type in component['types']:
					if hasattr(self, component_type):
						setattr(self, component_type, component['long_name'])

	def update_all_location(self, address, **kwargs):
		""" Met à jour tous les éléments de l'adresse."""
		default_GPS = kwargs.get('default_GPS', None)
		try:
			if not settings.USE_GMAPS:
				raise Exception("No use of Gmaps")
			self.nbr_updates_of_all_location += 1
			geocode = self.get_geocode(address=address)
			self.update_formatted_address(geocode=geocode)
			self.update_address_components(geocode=geocode)
			self.update_geo_coord(geocode=geocode)
		except Exception as e:
			#print("Une erreur inattendue est survenue dans location/update_all :", e)
			if default_GPS: 
				self.geo_coord = Point(default_GPS.x, default_GPS.y, srid=4326)
			else: 
				self.geo_coord = Point(2.333333, 48.866667, srid=4326)


class ClientProfile(Location, ClientCagnotteManager, ClientGamelleManager):

	class Meta:
		verbose_name = 'Profil client'
		verbose_name_plural = 'Profils clients'

	def __str__(self):
		return "Profil de {}".format(self.user)

	user = models.OneToOneField(
		User, verbose_name='Utilisateur associé',
		on_delete=models.CASCADE, related_name='client_profile', default=None)
	phone_number = PhoneNumberField()



class RestaurantProfile(Location, RestaurantGamelleManager):

	class Meta:
		verbose_name = 'Profil restaurateur'
		verbose_name_plural = 'Profils restaurateurs'

	def __str__(self):
		return "Profil restaurateur de {}".format(self.user)

	user = models.OneToOneField(
		User, verbose_name='Utilisateur associé',
		on_delete=models.CASCADE, related_name='restaurant_profile', default=None)
	code = models.CharField(
		'code restaurateur', max_length=10, default="INVALID",
		blank=True, null=True)
	nbr_clean_gamelles = models.PositiveSmallIntegerField(
		'nombre de gamelles propres', default=0)

	def is_code_submitted_correct(self, code_submitted):
		return code_submitted.lower() == self.code.lower()
