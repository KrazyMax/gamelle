#-*- coding: utf-8 -*-

import copy
import datetime

from allauth.account.forms import SignupForm
from django import forms
from django.contrib import messages
from django.contrib.auth.models import Group, User
from django.core.exceptions import ValidationError
from django.db.models import BooleanField, PositiveSmallIntegerField, Value
# from django.db.models.expressions import Case, When
# from django.db.models.functions import Concat
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.formfields import PhoneNumberField
#from tempus_dominus.widgets import DatePicker
from .models import ClientProfile
from . import exceptions

display_for_user_select = {
	'class': 'selectpicker',
	'data-style': 'btn-light px-2',
	'data-width': '90%',
	'data-live-search': 'true',
	'data-live-search-placeholder': "Taper quelques lettres pour rechercher..."
}
# display_for_user_multiselect = copy.deepcopy(display_for_user_select)
# display_for_user_multiselect.update({
# 	'multiple': True
# })
# datepicker_attrs = { 'append': 'fa fa-calendar', 'size': 'large' }

#--------------------------------------------------------------------
class ClientProfile_Mixin(forms.Form):

	class Meta:
		abstract = True

	first_name = forms.CharField(
		max_length=50, label='Prénom', widget=forms.TextInput({
			'class': 'form-control form-control-lg',
			'placeholder': 'Votre prénom'
		}))
	last_name = forms.CharField(
		max_length=50, label='Nom de famille', widget=forms.TextInput({
			'class': 'form-control form-control-lg',
			'placeholder': 'Votre nom'
		}))
	email = forms.EmailField(
		widget=forms.EmailInput({
			'class': 'form-control form-control-lg',
			'placeholder': 'Votre adresse mail',
		}))
	phone_number = PhoneNumberField(
		widget=forms.TextInput({
			'class': 'form-control form-control-lg',
			'placeholder': 'Votre numéro de téléphone'
		}))
	
	def clean_first_name(self):
		return self.cleaned_data["first_name"].title()

	def clean_last_name(self):
		return self.cleaned_data["last_name"].upper()

	# def clean_phone_number(self):
	# 	"""
	# 	TODO : peut-être rien à faire avec le module phonenumber, à tester en pratique
	# 	"""
	# 	return self.cleaned_data["phone_number"]

class CustomSignup_Form(ClientProfile_Mixin, SignupForm):

	# NOTE : à changer
	# pas de username à renseigner à l'inscription, 
	# celui-ci est défini automatiquement sur la base prénom.nom

	def save(self, request):
		try:
			#------------------------------------------------------------------
			# par défaut
			user = super(CustomSignup_Form, self).save(request)
			#------------------------------------------------------------------
			# on met à jour les attributs de user
			user.first_name = self.cleaned_data['first_name']
			user.last_name = self.cleaned_data['last_name']
			#user.username = self.cleaned_data['first_name'].lower() + '.' + self.cleaned_data['last_name'].lower()
			# on associe le groupe RegularAccount par défaut
			#user.groups.add(Group.objects.get(name='RegularAccount'))
			user.save()
			#------------------------------------------------------------------
			# on créé le profil utilisateur associé
			new_profile = ClientProfile(user=user, phone_number=self.cleaned_data["phone_number"])
			new_profile.save()
			print("TEL VAUT : ", new_profile.phone_number)
			#------------------------------------------------------------------
			user.save()
			return user
		
		except Exception as e:
			print("Problème dans l'inscription d'un utilisateur (CustomSignup_Form) : ", e)
			try:
				u = User.objects.get(username=self.cleaned_data.get('username'))
				u.delete()
			except User.DoesNotExist: 
				print("Aucun utilisateur avec le username {}.".format(self.cleaned_data.get('username')))
			except Exception as e:
				raise e # message d'erreur personnalisé à réaliser

class UpdateClientProfile_Form(ClientProfile_Mixin):

	def __init__(self, user, *args, **kwargs):
		self.user = user
		super().__init__(*args, **kwargs)

	def clean_email(self):
		""" On vérifie qu'on ne choisit pas le même email qu'un autre user. """
		new_email = self.cleaned_data['email']
		try:
			if self.user == User.objects.get(email=new_email):
				return new_email			
			else:
				raise forms.ValidationError(
					_("Cet email est déjà utilisé par un autre utilisateur."),
					code='email_already_taken'
				)
		except User.DoesNotExist:
			return new_email

	def clean(self):
		cleaned_data = super(UpdateClientProfile_Form, self).clean()
		return cleaned_data

	def save(self):
		UserToUpdate = User.objects.get(username=self.user.username)
		ClientProfileToUpdate = UserToUpdate.client_profile
		#-----------------------
		UserToUpdate.username = self.cleaned_data['first_name'].lower() + '.' + self.cleaned_data['last_name'].lower()
		UserToUpdate.first_name = self.cleaned_data['first_name']
		UserToUpdate.last_name = self.cleaned_data['last_name']
		UserToUpdate.email = self.cleaned_data['email']
		#-----------------------
		ClientProfileToUpdate.team = self.cleaned_data['team']
		#-----------------------
		UserToUpdate.save()
		ClientProfileToUpdate.save()


class DealWithGamelle_Form(forms.Form):
	""" Formulaire permettant d'utiliser, rendre ou échanger une gamelle """

	action = forms.CharField(max_length=10)

	restaurant_to_select = forms.ModelChoiceField(
		queryset=User.objects.filter(restaurant_profile__isnull=False),
		widget=forms.Select({
            'class': 'selectpicker',
            'data-style': 'btn-light px-2 mx-auto',
            'data-width': '100%',
            'data-live-search': 'true',
            'data-live-search-placeholder': "Taper quelques lettres pour rechercher..."
        })
	)

	# code_to_submit = forms.CharField(
	# 	max_length=15, label='Code restaurateur', 
	# 	widget=forms.TextInput({
	# 		'class': 'form-control form-control-lg',
	# 		'placeholder': 'Code restaurateur'
	# 	}))
	
	nbr_gamelles = forms.IntegerField(min_value=0, max_value=1000, initial=0)

	def __init__(self, *args, **kwargs):
		# NOTE : le pop doit bien avoir lieu avant d'appeler super,
		# car la méthode super ne reconnaîtrait pas user comme kwargs sinon
		self.user = kwargs.pop('user', None)
		if not self.user:
			raise exceptions.DontKnowWhoFillsForm_Error()
		super(DealWithGamelle_Form, self).__init__(*args, **kwargs)

	def clean(self):
		cleaned_data = super(DealWithGamelle_Form, self).clean()
		ValidationErrorList = []
		#code_to_submit = cleaned_data.get("code_to_submit")
		restaurant_to_select = cleaned_data.get("restaurant_to_select")

		# if not restaurant_to_select.restaurant_profile.is_code_submitted_correct(code_to_submit):
		# 	ValidationErrorList.append(ValidationError(
		# 		_("Le code restaurateur ne correspond pas."),
		# 		code="restaurant_code_does_not_match"
		# 	))

		if ValidationErrorList != []:
			raise ValidationError(ValidationErrorList)

		return cleaned_data
