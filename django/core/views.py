from django.contrib import messages
from django.contrib.auth.mixins import (AccessMixin, LoginRequiredMixin,
                                        PermissionRequiredMixin)
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse, reverse_lazy
from django.views.generic import (CreateView, DeleteView, DetailView, FormView,
                                  ListView, RedirectView, TemplateView,
                                  UpdateView)

from . import exceptions
from . import forms as gamelle_forms
from . import messages as gamelle_messages
from django.conf import settings

class Home_View(TemplateView):
	template_name = 'core/home.html'


# --------------------------------------------------------------
class UpdateClientProfile_View(LoginRequiredMixin, FormView):
	"""
	Vue qui permet de mettre à jour le profil utilisateur.
	Seul le mot de passe n'est pas mis à jour par l'intermédiaire de cette vue :
	on utilise plutôt la vue fournie avec django-allauth.
	"""
	form_class = gamelle_forms.UpdateClientProfile_Form
	template_name = 'account/update_client_profile.html'
	success_url = reverse_lazy("update_client_profile")
	success_message = gamelle_messages.SUCCESS_UPDATE_USER_PROFILE

	def form_valid(self, form):
		try:
			form.save()
			messages.success(self.request, self.success_message, extra_tags='safe')
			return super().form_valid(form)
		except Exception as e:
			print("Erreur inattendue dans UpdateClientProfile_View : ", e)
			messages.error(self.request, nounou_messages.ERROR_M_UNEXPECTED, extra_tags='safe')

	def get_form_kwargs(self):
		""" Return the keyword arguments for instantiating the form. """
		kwargs = super().get_form_kwargs()
		kwargs['user'] = self.request.user
		return kwargs

	def get_initial(self):
		initial = {
			'username': self.request.user.username,
			'first_name': self.request.user.first_name,
			'last_name': self.request.user.last_name,
			'email': self.request.user.email, 
			'phone_number': self.request.user.client_profile.phone_number
		}
		return initial

class DealGamelle_View(LoginRequiredMixin, FormView):
	template_name = 'core/deal_gamelle.html'
	success_url = reverse_lazy('home')
	form_class = gamelle_forms.DealWithGamelle_Form
	success_message = {
		'use': gamelle_messages.SUCCESS_USE_GAMELLE,
		'give_back': gamelle_messages.SUCCESS_GIVE_BACK_GAMELLE,
		'swap': gamelle_messages.SUCCESS_SWAP_GAMELLE
	}

	def get_form_kwargs(self):
		kwargs = super().get_form_kwargs()
		if hasattr(self, 'object'):
			kwargs.update({'instance': self.object})
		kwargs.update({'user': self.request.user})
		return kwargs

	def form_valid(self, form):
		# messages.success(self.request, self.success_message, extra_tags='safe')
		# print(form.cleaned_data)
		try:
			nbr_gamelles = form.cleaned_data['nbr_gamelles']
			client_profile = self.request.user.client_profile
			restaurant_profile = form.cleaned_data['restaurant_to_select'].restaurant_profile

			if form.cleaned_data['action'] == 'use':
				amount_to_debit = nbr_gamelles * settings.GAMELLE_VALUE
				client_profile.debit(amount_to_debit)
				client_profile.borrow_gamelle(nbr_gamelles)
				restaurant_profile.give_gamelle(nbr_gamelles)
				client_profile.save()
				restaurant_profile.save()

			elif form.cleaned_data['action'] == 'give_back':
				amount_to_credit = nbr_gamelles * settings.GAMELLE_VALUE
				client_profile.credit(amount_to_credit)
				client_profile.bring_back_gamelle(nbr_gamelles)
				restaurant_profile.bring_back_gamelle(nbr_gamelles)
				client_profile.save()
				restaurant_profile.save()

			elif form.cleaned_data['action'] == 'swap':
				# TODO:  à venir
				pass

			else:
				# TODO : raise une exception ici
				pass

			messages.success(
				self.request, 
				self.success_message[form.cleaned_data['action']], 
				extra_tags='safe')
			return super().form_valid(form)
		
		except (
			exceptions.CantDebitIfNotEnoughBalance_Error, 
			exceptions.CantBringBackGamelleYouDontHave_Error,
			exceptions.NotEnoughGamellesToGive_Error) as e:
			messages.error(self.request, e.msg_for_user, extra_tags='safe')
		
		except Exception as e:
			print("Erreur inattendue dans DealGamelle_View : ", e)
			messages.error(self.request, gamelle_messages.ERROR_M_UNEXPECTED, extra_tags='safe')
		
		return redirect(self.request.META['HTTP_REFERER'])
		
		
			


	# def get_context_data(self, **kwargs):
	# 	context = super().get_context_data(**kwargs)
	# 	context['DealWithGamelle_Form'] = gamelle_forms.DealWithGamelle_Form
	# 	return context

# --------------------------------------------------------------
class BorrowGamelle_View(LoginRequiredMixin, RedirectView):
	pass
