import os

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from . import models


#--------------------------------------------------------------------
class ClientProfileInline(admin.StackedInline):
	model = models.ClientProfile
	list_display = ('user', 'current_balance')
	fk_name = 'user'

	def get_form(self, request, obj=None, **kwargs):
		form = super(ClientProfileAdmin, self).get_form(request, obj, **kwargs)
		form.base_fields['user'].queryset = User.objects.filter(profile__isnull=True)
		return form


class CustomUserAdmin(UserAdmin):
	inlines = (ClientProfileInline, )
	list_display = ['username', 'email', 'date_joined']
	ordering = ('username',)
	search_fields = ('username', 'email',)

	def get_inline_instances(self, request, obj=None):
		if not obj:
			return list()
		return super(CustomUserAdmin, self).get_inline_instances(request, obj)


class ClientProfileAdmin(admin.ModelAdmin):
	model = models.ClientProfile
	list_display = [
		'user',
		'nbr_gamelles_dirty', 'nbr_gamelles_brought_back', 'nbr_gamelles_used']

class RestaurantProfileAdmin(admin.ModelAdmin):
	model = models.RestaurantProfile
	list_display = (
		'user', 'code', 'nbr_clean_gamelles')


#--------------------------------------------------------------------
admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)

admin.site.register(models.ClientProfile, ClientProfileAdmin)
admin.site.register(models.RestaurantProfile, RestaurantProfileAdmin)
