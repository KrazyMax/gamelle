from core import messages

#-----------------------------------------------------------------------------#


class Error(Exception):
	""" Classe de base pour les exceptions """
	msg_for_user = "Message à destination de l'utilisateur (non renseigné)."
	msg_for_admin = "Message à destination de admin (non renseigné)."

	def __str__(self):
		# Par défaut, on renvoie le msg à destination de l'utilisateur
		# (on ne prend pas le risque des révéler des infos sensibles)
		return self.msg_for_user


#-----------------------------------------------------------------------------#
class CantDebitIfNotEnoughBalance_Error(Error):
	""" Exception levée si un client essaie de débiter plus que le montant de sa cagnotte. """
	def __init__(self):
		self.msg_for_user = messages.ERROR_DEBIT_NOT_ENOUGH_BALANCE


#-----------------------------------------------------------------------------#
class CantBringBackGamelleYouDontHave_Error(Error):
	""" Exception levée si un client essaie de rapporter plus de gamelles sales qu'il n'en possède """
	def __init__(self):
		self.msg_for_user = messages.ERROR_NOT_ENOUGH_DIRTY_GAMELLE


class NotEnoughGamellesToGive_Error(Error):
	""" Exception levée si un restaurant ne dispose pas d'assez de gamelles à prêter à un client """
	def __init__(self):
		self.msg_for_user = messages.ERROR_NOT_ENOUGH_CLEAN_GAMELLE
		


class DontKnowWhoFillsForm_Error(Error):
	""" Exception levée si aucun user n'est passé en paramètre d'un formulaire qui en a besoin. """
	def __init__(self):
		self.msg_for_user = messages.ERROR_DONT_KNOW_WHO_FILLS_FORM

		
