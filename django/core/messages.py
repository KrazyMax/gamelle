# --------------------------------------------------------------
# NOTE : SUCCESS
SUCCESS_UPDATE_USER_PROFILE = """
Vous avez bien mis à jour votre profil !
"""
SUCCESS_USE_GAMELLE = """
Vous avez emprunté une ou plusieurs gamelles avec succès !
"""
SUCCESS_GIVE_BACK_GAMELLE = """
Vous avez rendu une ou plusieurs gamelles avec succès !
"""
SUCCESS_SWAP_GAMELLE = """
Vous avez échangé une ou plusieurs gamelles avec succès !
"""


# --------------------------------------------------------------
# NOTE : ERROR
# ERROR
ERROR_M_UNEXPECTED = """
Une erreur inattendue est survenue, merci de réessayer ultérieurement.
"""
ERROR_DEBIT_NOT_ENOUGH_BALANCE = """
Le montant de votre cagnotte est trop bas pour réaliser cette action.
"""
ERROR_NOT_ENOUGH_DIRTY_GAMELLE = """
Vous ne pouvez pas ramener plus de gamelles sales que vous n'en possédez !
"""
ERROR_NOT_ENOUGH_CLEAN_GAMELLE = """
Le restaurant ne dispose pas du nombre de gamelles souhaitées.
"""
ERROR_DONT_KNOW_WHO_FILLS_FORM = """
Le formulaire ne connaît pas l'utilisateur courant.
"""
