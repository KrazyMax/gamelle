from django import template
from django.utils.safestring import mark_safe

register = template.Library()

#--------------------------------------------------------
@register.filter(name="placeholder")
def placeholder(value, arg):
	"""
	Permet de définir directement la valeur du placeholder
	"""
	value.field.widget.attrs["placeholder"] = arg
	return value

#--------------------------------------------------------
@register.filter(name="hidden")
def hidden(value):
	"""
	Permet de masquer un champ 
	"""
	value.field.widget.attrs["hidden"] = True
	return value
	
	
#--------------------------------------------------------
@register.filter(name="addClass")
def addClass(value, arg):
	"""
	Permet d'ajouter des classes de styles css
	"""
	value.field.widget.attrs["class"] = arg
	return value

#--------------------------------------------------------
@register.filter(name='isActive')
def isActive(path, url):
	"""	
	isActive permet de savoir si un lien correspond à la page active (sur laquelle on se trouve).
	Exemple : {{ url_home|isActive:request.path }} -> return True si url_home est l'url courante (active)
	"""
	if path == url: return "active"

#--------------------------------------------------------
@register.filter(name='has_group') 
def has_group(user, group_name):
    return user.groups.filter(name=group_name).exists()