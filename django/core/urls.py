from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

from core import views

urlpatterns = [
    path('deal-gamelle', views.DealGamelle_View.as_view(), name="deal_gamelle"),
]
