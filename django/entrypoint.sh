#!/bin/bash
python manage.py migrate
python manage.py collectstatic --no-input
gunicorn gamelle.wsgi:application --bind 0.0.0.0:80
